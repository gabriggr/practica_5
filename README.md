# PRÁCTICA 5 #

El objetivo de esta prátcia es aprender a usar joins ya agregaciones en spark.


## Tablón Tarjetas de crédito ##

En esta práctica vamos a realizar un tablón con la información de las tarjetas de crédito de los clientes.

* El tablón tiene que poblar la siguiente tabla.

```sql
   CREATE EXTERNAL TABLE IF NOT EXISTS m_operations.t_tablon_tarjeta(
   card_id Int COMMENT "Identificador de la tarjeta, origen t_card",
   issued string COMMENT "Identificador de la tarjeta, origen t_card",
   type string COMMENT "Tipo de tarjeta, origen t_card", 
   client_id Int COMMENT "Identificador del Cliente, origen t_client",
   name string COMMENT "Nombre del Cliente, origen t_client",
   surname string COMMENT "Apellido del Cliente, origen t_client", 
   phone Int COMMENT "Telf. del Cliente, origen t_client",
   account_id Int COMMENT "Id cuenta, origen t_account",
   frequency String COMMENT "Tipo cuenta, origen t_account", 
   district_id Int COMMENT "Id distrito , origen t_district",
   district_name string COMMENT "Nombre distrito , origen t_district",
   district_region string COMMENT "Region , origen t_district")
   STORED AS PARQUET TBLPROPERTIES ("parquet.compression"="SNAPPY");
```

### 1. Creamos  práctica 5 en nuestro workSpace###

Creamos el objeto de scala Practica5.scala, que situaremos en src/main/scala/Practica5.scala


```scala

import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions._

object Practica5 {

  //// Schemas
  val MOperationsSchema = "m_operations"
  val ROperationsSchema = "r_operations"

  //// TABLES
  // Tables Master
  val MTLoans = s"${MOperationsSchema}.t_loan"
  val MTClient = s"${MOperationsSchema}.t_client"
  val MTAccount = s"${MOperationsSchema}.t_account"
  val MTTransaction = s"${MOperationsSchema}.t_transaction"
  val MTOrder = s"${MOperationsSchema}.t_order"
  val MTDistrict = s"${MOperationsSchema}.t_district"
  val MTCard = s"${MOperationsSchema}.t_card"
  val MTDisp = s"${MOperationsSchema}.t_disp"

  // Tables Raw
  val RTLoans = s"${ROperationsSchema}.t_loan"
  val RTClient = s"${ROperationsSchema}.t_client"
  val RTAccount = s"${ROperationsSchema}.t_account"
  val RTTransaction = s"${ROperationsSchema}.t_transaction"
  val RTOrder = s"${ROperationsSchema}.t_order"
  val RTDistrict = s"${ROperationsSchema}.t_district"
  val RTCard = s"${ROperationsSchema}.t_card"
  val RTDisp = s"${ROperationsSchema}.t_disp"

  //// Configs
  val WriteRepartition = 4


  val StagingOut:String = "/staging/in/"
  
  
  
  def readMasterTable(table: String)(implicit spark: SparkSession): DataFrame = {
    spark.read.table(table)
  }

  def writeMasterTable(table: String, df: DataFrame)(implicit spark: SparkSession): Unit = {
    spark.sqlContext.setConf("hive.exec.dynamic.partition", "true")
    spark.sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")

    // Seleccionamos las columnas en el orden de la tabla
    val columns = spark.table(table).columns.map(col)

    // 1. Lectura de tablas, solo con los campos que se van a utilizar
    // 2. Joins entre las tablas utilizando los Ids que relacionan cada una de ellas
    df
      .select(columns: _*)
      .write
      .mode(SaveMode.Overwrite)
      .insertInto(table)
  }

  def loadTablon(implicit spark: SparkSession) = {
    import spark.implicits._
    val dfCard  = spark.read.table("m_operations.t_card")
    dfCard.show
    dfCard.printSchema()
    // writeMasterTable("m_operations.t_tablon_tarjeta", dfFinal)
  }

  def showMetrics(implicit spark: SparkSession)= {
    val df = readMasterTable("m_operations.t_tablon_tarjeta")
    df.show()

    // 1. Mostrar el número de tarjetas por region


    // 2. Mostrar el número de tarjetas por region y distrito orden ascendente


    // 3. Máxima y mínima fecha de emisión


    // 4. Máxima y mínima fecha de emision por tipo de tarjeta(type)


    // 5. Lista de regiones por tipo de cuenta(frequency)

  }
}



```

### 2. Clase principal el proceso ###

```scala
object AppCreateTablonClientCards extends SparkApp{

  Practica5.loadTablon
  Practica5.showMetrics

}
```

# EJERCICIO 5 #

En este ejericio se tiene que crear el tablón de clientes, que contendrá información agragada de las cuentas del cliente, totales transaciones, totales loans, orders por cuenta



| Campo	         |Descripción   	                                 |Tabla   Origen |
|----------------|---------------------------------------------------|---------------|
|client_id       |Formato original                                   |t_client       |
|birth	         |String formato yyyyMM                              |t_client       |
|sex             |Formato original                                   |t_client       |
|name            |Formato original                                   |t_client       |
|surname	     |Formato original                                   |t_client       |
|phone	         |Formato original                                   |t_client       |
|                |                                                   |               |
|account_id	     |Formato original                                   |t_account      |
|frequency	     |Formato original                                   |t_account      |
|date	         |String formato yyyyMM                              |t_account      |
|                |                                                   |               |
|total_amount_loan|Para loans con status diferentes de A ó B	         |t_loan         |
|av_payments_loan|Media de los pagos(payments)                       |t_loan	     |
|                |                                                   |               |
|num_tran	     |Número de transaciones realizadas por la cuenta    |t_transaction  |
|total_amount_tran|Total amount de las transacciones de la cuenta      |t_transaction  |
|total_balace_tran|Total Balance de las transacciones de la cuenta     |t_transaction  |
|av_balance_tran |Balance medio de la cuenta	              |t_transaction  |
|VYDAJ	         |Total de operaciones realizadas de este tipo(type) |t_transaction  |
|PRIJEM	         |Total de operaciones realizadas de este tipo(type) |t_transaction  |
|VYBER	         |Total de operaciones realizadas de este tipo(type) |t_transaction  |
|		         |                                                   |               |
|total_amount_order|Suma de los Amount de las orders de una cuenta	 |t_order        |
|		         |                                                   |               |
|name_dist	         |Formato original	                                 |t_district     |            
|region_dist	         |Formato original	                                 |t_district     |
|average_salary_dist	 |A11	                                             |t_district     |




** Tablón de salida **


```sql
CREATE EXTERNAL TABLE t_tablon_cliente(
client_id int COMMENT 'Identificador cliente',
birth string COMMENT 'Fecha nacimiento cliente',
sex string COMMENT 'Genero',
name string COMMENT 'Nombre del cliente',
surname string COMMENT 'Apellido del cliente',
phone int COMMENT 'Teléfono del cliente',
account_id int COMMENT 'Identificador de cuenta',
frequency string COMMENT 'Tipo de cuenta',
date string COMMENT 'Fecha de creación de la cuenta',
total_amount_loan decimal(10,2) COMMENT 'Total amount de los loans de la cuenta',
av_payments_loan  decimal(10,2) COMMENT 'Promedio de los loans de la cuenta',
num_tran int COMMENT 'Número de transacciones realizadas por la cuenta',
total_amount_tran decimal(10,2) COMMENT 'Total amount de las transacciones de la cuenta',
total_balace_tran decimal(10,2) COMMENT 'Total Balance de las transacciones de la cuenta',
av_balance_tran decimal(10,2) COMMENT 'Balance medio de la cuenta',
VYDAJ int COMMENT 'Total de operaciones realizadas de tipo=VYDAJ',
PRIJEM int COMMENT 'Total de operaciones realizadas de tipo=PRIJEM',
VYBER int COMMENT 'Total de operaciones realizadas de tipo=VYBER',
total_amount_order decimal(10,2) COMMENT 'Suma de los Amount de las orders de una cuenta',
name_dist string COMMENT 'Nombre del distrito para la cuenta del cliente',
region_dist string COMMENT 'Región del distrito de la cuenta',
average_salary_dist decimal(10,2) COMMENT 'Salario medio del distrito de la cuenta del cliente') COMMENT 'Tabla de datos agregados del cliente por cuenta'
STORED AS PARQUET TBLPROPERTIES ("parquet.compression"="SNAPPY");
```





